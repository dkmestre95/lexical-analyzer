#ifndef INTERPRET_H_
#define INTERPRET_H_

#include "parse.h"
#include "symtable.h"

void interpretTree(ParseTree* p);

void interpretStmt(ParseTree *p);

void interpretIf(ParseTree *p);

bool isBoolExpr(ParseTree*p);

void interpretPrint(ParseTree *p);

void interpretExpr(ParseTree *p);

bool interpretLogicExpr(ParseTree *p);

bool interpretCompareExpr(ParseTree *p);

bool compareIdents(ParseTree *p, CompareType ct);

void interpretOperation(ParseTree *p, Value *v);

void printOperation(ParseTree *p);

int interpretInts(ParseTree *p);

int interpretInts(Value *v, ParseTree *p);

int interpretInts(Value *v1, Value *v2, ParseTree *p);

string interpretStrings(ParseTree *p);

string interpretStrings(Value *v, ParseTree *p);

string interpretStrings(Value *v1, Value *v2, ParseTree *p);

void interpretError(int line, string err);

bool interpretUnaryMinus(ParseTree *p);

#endif
