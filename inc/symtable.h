#ifndef SYMTABLE_H_
#define SYMTABLE_H_

#include "parse.h"

enum CompareType{
    isEQ,
    isNEQ,
    isGT,
    isGEQ,
    isLT,
    isLEQ,
    isErr
};

enum ValueType{
    isInt,
    isStr,
    isBool,
    isError
};

typedef struct {
    ValueType vt = isError;

    string sval;
    bool bval;
    int ival;
} Value;

struct Symbol{
    Value *v;
    string name;
    Symbol* next = 0;
};

typedef struct Symbol Symbol;

#ifdef SYMBOLTABLE
Symbol* head = 0;
#else
extern Symbol* head;
#endif

/* Create a symbol based on the data contained in a parse tree node*/
Symbol* createSymbol(ParseTree *p);

/* Appends the given symbol s to the symbol table */
void insertSymbol(Symbol* s);

/* Find the given Symbol s inside of symbol table and update its value based on its type */
void updateSymbol(Symbol* s);

/* Checks symbol table if the symbol s exists by name */
bool hasSymbol(string name);
                       
/* Find the symbol by name and print something based on its type */
void printSymbol(string name);

/* Traverse symbol table for the symbol matching the given name, then return its value based on its value type */
Value* getValue(string name);

#endif
