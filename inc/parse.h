#ifndef PARSE_H_
#define PARSE_H_

#include <iostream>

using namespace std;

#include "tokens.h"
#include "parsetree.h"

ParseTree *Prog(istream *in, int *line);
ParseTree *Slist(istream *in, int *line);
ParseTree *Stmt(istream *in, int *line);
ParseTree *IfStmt(istream *in, int *line);
ParseTree *PrintStmt(istream *in, int *line);
ParseTree *Expr(istream *in, int *line);
ParseTree *LogicExpr(istream *in, int *line);
ParseTree *CompareExpr(istream *in, int *line);
ParseTree *AddExpr(istream *in, int *line);
ParseTree *MulExpr(istream *in, int *line);
ParseTree *Factor(istream *in, int *line);
ParseTree *Primary(istream *in, int *line);

#endif
