/*
 * tokens.h
 *
 * CS280
 * Fall 2018
 */

#ifndef TOKENS_H_
#define TOKENS_H_

#include <string>
#include <iostream>
using std::string;
using std::istream;
using std::ostream;

enum TokenType {
		// keywords
	PRINT,
	IF,
	THEN, //2
	TRUE, //3
	FALSE, //4

		// an identifier
	IDENT, //5

		// an integer and string constant
	ICONST, //6
	SCONST, //7

		// the operators, parens and semicolon
	PLUS,
	MINUS,
	STAR,
	SLASH,
	ASSIGN,
	EQ,
	NEQ,
	LT,
	LEQ,
	GT,
	GEQ,
	LOGICAND,
	LOGICOR,
	LPAREN, //21
	RPAREN, //22
	SC,

		// any error returns this token
	ERR,

		// when completed (EOF), return this token
	DONE //25
};

class Token {
	TokenType	tt;
	string		lexeme;
	int			lnum;

public:
	Token() {
		tt = ERR;
		lnum = -1;
	}
	Token(TokenType tt, string lexeme, int line) {
		this->tt = tt;
		this->lexeme = lexeme;
		this->lnum = line;
	}

	bool operator==(const TokenType tt) const { return this->tt == tt; }
	bool operator!=(const TokenType tt) const { return this->tt != tt; }

	TokenType	GetTokenType() const { return tt; }
	string		GetLexeme() const { return lexeme; }
	int			GetLinenum() const { return lnum; }
};

extern ostream& operator<<(ostream& out, const Token& tok);

extern Token getNextToken(istream *in, int *linenum);


#endif /* TOKENS_H_ */
