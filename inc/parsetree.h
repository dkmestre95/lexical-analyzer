#ifndef PARSETREE_H_
#define PARSETREE_H_

#include <vector>
#include <map>
using std::vector;
using std::map;
using std::string;

// NodeType represents all possible types
enum NodeType {
	       ERRTYPE,
	       INTTYPE,
	       STRTYPE,
	       BOOLTYPE
};

struct ParseTree{
  ParseTree* l;
  ParseTree* r;

  NodeType n = ERRTYPE;

  Token t;

  int iconst;
  string sconst;
  bool bconst;
};

int LeafCount(ParseTree *p);

int StringCount(ParseTree *p);

int IdentCount(ParseTree *p, map<string, int>& m);

bool HasErrType(ParseTree *p);

#endif
