#include "../inc/interpret.h"

Symbol* createSymbol(ParseTree *p){

    Symbol *s = new Symbol;
    Symbol *s2 = 0;
    Value *v = new Value;

    //for an assignment operation, the left child of p contains the variable
    //and the right child contains the value.
    //check the node type of the right child to get variable type

    switch(p->r->n){
        case INTTYPE:
            v->ival = p->r->iconst;
            v->vt = isInt;
            break;
        case STRTYPE:
            v->sval = p->r->sconst;
            v->vt = isStr;
            break;
        case BOOLTYPE:
            v->bval = p->r->bconst;
            v->vt = isBool;
            break;
        case ERRTYPE:
            //There is an operation stored in its token
            //find out which it is and act accordingly
            switch(p->r->t.GetTokenType()){
                case PLUS:
                case MINUS:
                case STAR:
                case SLASH:
                    interpretOperation(p->r,v);
                    break;
                case ASSIGN:{
                    //TODO: consider possibility that this symbol already exists in symtable
                    s2 = createSymbol(p->r);
                    insertSymbol(s2);
                    break;
                }
                case LT:
                case GT:
                case LEQ:
                case GEQ:
                case EQ:
                case NEQ:
                    v->bval = interpretCompareExpr(p->r);
                    v->vt = isBool;
                    break;
                case LOGICOR:
                case LOGICAND:
                    v->bval = interpretLogicExpr(p->r);
                    v->vt = isBool;
                default:
                    break;
            }
            break;
    }

    s->name = p->l->t.GetLexeme();
    //set s's value based on whether it was set equal to another symbol
    if(s2){
        switch(s2->v->vt){
            case isInt:
                v->ival = s2->v->ival;
                v->vt = isInt;
                break;
            case isStr:
                v->sval = s2->v->sval;
                v->vt = isStr;
                break;
            case isBool:
                v->bval = s2->v->bval;
                v->vt = isBool;
                break;
            case isError:
                break;
        }
    }

    s->v = v;

    return s;
}

void insertSymbol(Symbol* s){
    Symbol* tmp;
    
    //If list is empty
    if(!head){
       head = s;
       return;
    }

    //otherwise, put s at the end of the list
    tmp = head;

    while(tmp->next){
        tmp = tmp->next;
    }

    tmp->next = s;
}
/* Check symbol table for a symbol with the name given via the input */
bool hasSymbol(string name){
    Symbol* tmp;

    tmp = head;

    while(tmp){
        if(tmp->name == name){
            return true;
        }else{
            tmp = tmp->next;
        }
    }
    return false;
}

/* PRE: Symbol table is not empty AND a symbol with the input s's name exists in the symbol table
 * POST: The symbol head representing what is already in the symbol table will now point to s */
void updateSymbol(Symbol* s){
    Symbol* tmp = head;

    while(tmp){
        if(tmp->name == s->name){
            tmp->v = s->v;
            delete(s);
            return;
        }
        tmp = tmp->next;
    }
}

/* PRE: The symbol with the given name already exists in the symbol table
 * POST: The symbol's value will be printed to stdout */
void printSymbol(string name){
    /* cout << "I'm printing something" << endl; */
   Symbol* tmp = head;

   while(tmp){
        if(tmp->name == name){
            switch(tmp->v->vt){
                case isInt:
                    cout << tmp->v->ival << endl;
                    return;
                case isStr:
                    cout << tmp->v->sval << endl;
                    return;
                case isBool:
                    cout << tmp->v->bval << endl;
                    return;
                case isError:
                    return;
            }
        }
        tmp = tmp->next;
   }
}

/* PRE: the symbol with the given name exists in the symbol tabe */
Value* getValue(string name){
    Symbol* tmp = head;

    while(tmp){
        if(tmp->name == name){
            break;
        }else{
            tmp = tmp->next;
        }
    }

    return tmp->v;
}
