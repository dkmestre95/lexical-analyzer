#include "../inc/interpret.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[]){

    ifstream file;
    istream *in = &cin;
    int linum(0);
    ParseTree *ptree = 0;
    map<string, int> idents;


    if(argc > 2){
        cerr << "TOO MANY FILENAMES" << endl;
        return -1;
    }else if(argc == 2){
        //open file and check if it's valid

        file.open(argv[1]);

        if(!file.good()){
            cerr << "COULD NOT OPEN " << argv[1] << endl;
            return -1;
        }else{
            in = &file;
        }

        //create the parsetree
        ptree = Prog(in, &linum);

        if(!ptree){
            return 0; //exit
        }

    }else{
        //use cin for input
        ptree = Prog(in, &linum);

    }
    
    //Interpret the parse tree
    interpretTree(ptree);

    return 0;
}
