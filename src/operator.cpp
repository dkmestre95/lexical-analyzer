#include "../inc/tokens.h"
#include <ostream>
#include <map>

ostream& operator<<(ostream& out, const Token& tok){
  //make a map of each token type into a string

  switch((int)tok.GetTokenType()){
    //keywords
  case 0:
    out << "PRINT";
    break;
  case 1:
    out << "IF";
    break;
  case 2:
    out << "THEN";
    break;
  case 3:
    out << "TRUE";
    break;
  case 4:
    out << "FALSE";
    break;
    //identifier
  case 5:
    out << "IDENT";
    break;
    //integer and string constants
  case 6:
    out << "ICONST";
    break;
  case 7:
    out << "SCONST";
    break;
    //operators, parens, and semicolon
  case 8:
    out << "PLUS";
    break;
  case 9:
    out << "MINUS";
    break;
  case 10:
    out << "STAR";
    break;
  case 11:
    out << "SLASH";
    break;
  case 12:
    out << "ASSIGN";
    break;
  case 13:
    out << "EQ";
    break;
  case 14:
    out << "NEQ";
    break;
  case 15:
    out << "LT";
    break;
  case 16:
    out << "LEQ";
    break;
  case 17:
    out << "GT";
    break;
  case 18:
    out << "GEQ";
    break;
  case 19:
    out << "LOGICAND";
    break;
  case 20:
    out << "LOGICOR";
    break;
  case 21:
    out << "LPAREN";
    break;
  case 22:
    out << "RPAREN";
    break;
  case 23:
    out << "SC";
    break;
  case 24:
    out << "ERR";
    break;
  case 25:
    out << "DONE";
    break;
  }

  return out;
}
