#define SYMBOLTABLE
#include "../inc/interpret.h"

void interpretError(int line, string err){
    cout << line << ": " << err << endl;
    exit(-1);
}

void interpretTree(ParseTree *p){
    //This assumes we've populated symbol table beforehand
    //Perform pre-order traversal
    //Base cases
    if(!p)
        return; 
    
    //By default all token types will be TokenType[0] (PRINT)
    //So we need to check if the lexeme is blank or not
    //If it's blank, continue with the traversal
    if(p->t.GetLexeme() == ""){
       interpretTree(p->l);
       interpretTree(p->r);
       return;
    }

    //Check the token of parent
    switch(p->t.GetTokenType()){
        case IF:
            interpretIf(p);
            break;
        case PRINT:
            interpretPrint(p->r);
            break;
        case ASSIGN:
            interpretExpr(p);
            break;
        default:
            break;
    }
}

void interpretPrint(ParseTree *p){
    
    if(p->n == BOOLTYPE){
        //check all token types that have been designated as BOOLTYPE
        switch(p->t.GetTokenType()){
            case TRUE:
                cout << "True" << endl;
                break;
            case FALSE:
                cout << "False" << endl;
                break;
            case IDENT:
                //Check if IDENT exists in symbol table 
                if(hasSymbol(p->t.GetLexeme())){
                   printSymbol(p->t.GetLexeme());
                }else{
                    interpretError(p->t.GetLinenum(), "RUNTIME ERROR undeclared variable " + p->t.GetLexeme());
                }
            default:
                break;
        }
    }else if(p->n == INTTYPE){
        cout << p->iconst << endl; 
    }else if(p->n == STRTYPE){
        cout << p->sconst << endl;
    }else{
        //p->n is an ERRTYPE, i.e anything else like a +, -, *, /, &&, ||, etc
        //check the Expr if its a more full expression
        switch(p->t.GetTokenType()){
            case LOGICOR:
            case LOGICAND:
                if(interpretLogicExpr(p)){
                    cout << "True" << endl;
                }else{
                    cout << "False" << endl;
                }
                break;
            case EQ:
            case NEQ:
            case LEQ:
            case GEQ:
            case LT:
            case GT:
                if(interpretCompareExpr(p)){
                    cout << "True" << endl;
                }else{
                    cout << "False" << endl;
                }
                break;
            case PLUS:
            case MINUS:
            case STAR:
            case SLASH:
                printOperation(p);
                break;
            default:
                break;
        }
    }
}

void interpretIf(ParseTree *p){
    //First check the validity of the left child (the expr)
    //Handle if its an explicit boolean
    if(p->l->n == BOOLTYPE){
        //first check if the left child is an ident
        if(p->l->t.GetTokenType() == IDENT){
            //check if its a boolean value
            if(getValue(p->l->t.GetLexeme())->vt == isBool){
                //if its bool value is true, interpret its statement
                if(getValue(p->l->t.GetLexeme())->bval){
                    interpretTree(p->r);
                }else{
                    return; 
                }
            }else{
                interpretError(p->l->t.GetLinenum(),"RUNTIME ERROR if expression is not boolean typed");
            }
        }
        //the left child at this point is a boolean
        //if it's false, do nothing
        //otherwise continue with program and interpret its statement
        if(!(p->l->bconst)){
            return;
         }
    }else if(p->l->n == INTTYPE || p->l->n == STRTYPE){
        interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR if expression is not boolean typed");
    }else{
        //left child's expr needs to be interpeted first to determine
        //if it's a valid boolean expression or not
    
    }

    //Interpret the statement held in the right child
    interpretTree(p->r);
}

void interpretExpr(ParseTree *p){
    //First consider what type of expression it is
    
    switch(p->t.GetTokenType()){
        case ASSIGN:
            //create symbol for the var stored in the left child
            //first check if the symbol table has the symbol already in it
            if(hasSymbol(p->l->t.GetLexeme() )){
                updateSymbol(createSymbol(p));
            }else{
                //check if it's a valid assignment by checking if the left child is an IDENT
                if(p->l->t.GetTokenType() != IDENT){ 
                    interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR invalid assignment"); 
                }else{ 
                    insertSymbol(createSymbol(p));
                } 
            }
            break;
        default:
            break;
    }
}

bool isBoolExpr(ParseTree *p){
    //STUB
    return true; 
}

bool interpretLogicExpr(ParseTree *p){
    //p is the parent of the logic expr
    //p->l contains the lefthand most expression
    //p->r contains the righthand most expression
    
    //TODO: Check for parenthetical operations on both sides

    if(p->t.GetTokenType() == LOGICOR){
        //check left child first
        switch(p->l->t.GetTokenType()){
            case TRUE:
                return true;
            case FALSE:
                break;
            case IDENT:
                //check if the ident exists
                if(hasSymbol(p->l->t.GetLexeme())){
                    //check if it's a boolean
                    if(getValue(p->l->t.GetLexeme())->vt == isBool){
                        if(getValue(p->l->t.GetLexeme())->bval){
                            return true; 
                        }else{
                            break; 
                        }
                    }else{
                        interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR first operand of || is not boolean typed");
                    }
                }else{
                    interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR undeclared variable " + p->l->t.GetLexeme());
                }
            case EQ:
            case NEQ:
            case LT:
            case LEQ:
            case GT:
            case GEQ:
                if(interpretCompareExpr(p->l)){
                    return true;
                }
                break;
            default:
                interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR first operand of || is not boolean typed");
                break;
        }
        
        //then check right child
        switch(p->r->t.GetTokenType()){
            case LPAREN:
                return interpretLogicExpr(p->r->r);
            case LOGICOR:
            case LOGICAND:
                return interpretLogicExpr(p->r);
            case TRUE:
                return true;
            case FALSE:
                return false;
            case IDENT:
                //check if the ident exists
                if(hasSymbol(p->r->t.GetLexeme())){
                    //check if it's a boolean
                    if(getValue(p->r->t.GetLexeme())->vt == isBool){
                        return getValue(p->r->t.GetLexeme())->bval;
                    }else{
                        interpretError(p->r->t.GetLinenum(), "RUNTIME ERROR first operand of || is not boolean typed");
                    }
                }else{
                    interpretError(p->r->t.GetLinenum(), "RUNTIME ERROR undeclared variable " + p->l->t.GetLexeme());
                }
            case EQ:
            case NEQ:
            case LT:
            case LEQ:
            case GT:
            case GEQ:
                return interpretCompareExpr(p->r);
            default:
                interpretError(p->r->t.GetLinenum(), "RUNTIME ERROR first operand of || is not boolean typed");
                break;
        }
    }else{
        //it's a LOGICAND
        //check the token type of the left and right children and handle them accordingly
        
        //check left child first
        switch(p->l->t.GetTokenType()){
            case TRUE:
                break;
            case FALSE:
                return false;
            case IDENT:{
                //get the value of the ident
                if(getValue(p->l->t.GetLexeme())->bval){
                    break;
                }else{
                    return false;
                }
            }
            case EQ:
            case NEQ:
            case LT:
            case LEQ:
            case GT:
            case GEQ:
                if(!interpretCompareExpr(p->l)){
                    return false; 
                }
                break;
            default:
                //TODO: Error handling
                break;
        }
        //then check right child
        switch(p->r->t.GetTokenType()){
            case LPAREN:
                return interpretLogicExpr(p->r->r);
            case LOGICOR:
            case LOGICAND:
                return interpretLogicExpr(p->r);
            case TRUE:
                return true;
            case FALSE:
                break;
            case IDENT:
                //return the value of the ident
                return getValue(p->r->t.GetLexeme())->bval;
            case EQ:
            case NEQ:
            case LT:
            case LEQ:
            case GT:
            case GEQ:
                return interpretCompareExpr(p->r);
            default:
                //TODO: Error handling
                break;
        }
    }

    return false;
}

bool interpretCompareExpr(ParseTree *p){
    //the node at p holds the operation
    //p->l holds the leftmost expression to be compared
    //p->r holds the rightmost expression to be compared
    
    //consider if they are both integers, strings, or booleans
    //also consider if both items being compared are variables
    //both items must be of the same type
    
    //check if the right child also contains its own compare expr
    //for more involved operations on the righthand side, need to make sure
    //that all things on the left and right are of the same type
    bool bothInts = false,
         bothStrs = false,
         bothIdents = false,
         bothBools = false;

    if(p->r->l){
        if(p->l->n == INTTYPE && p->r->l->n == INTTYPE ){
            bothInts = true; 
        }else if(p->l->n == STRTYPE && p->r->l->n == STRTYPE){
            bothStrs = true; 
        }else if(p->l->t.GetTokenType() == IDENT && p->r->l->t.GetTokenType() == IDENT){
            bothIdents = true; 
        }else if(p->l->n == BOOLTYPE && p->r->l->n == BOOLTYPE){
            bothBools = true;
        }else{
            //TODO: Error handling 
        }
    }
    
    switch(p->r->t.GetTokenType()){
        case LT:
            //make p->r->l->n into a bool type and set it equal to the comparison
            p->r->l->n = BOOLTYPE;
            if(bothInts){
                p->r->l->bconst = p->l->iconst < p->r->l->iconst;
            }else if(bothStrs){
            
            }else if(bothBools){
            
            }else if(bothIdents){
                //check if idents exist
                //then check what values they are
                //then perform comparison on those values
                //set p->r->l->bconst equal to the result of said comparison
            }
            return interpretCompareExpr(p->r);
        case GT:
            p->r->l->n = BOOLTYPE;
            p->r->l->bconst = p->l->iconst > p->r->l->iconst;
            return interpretCompareExpr(p->r);
        case LEQ:
            p->r->l->n = BOOLTYPE;
            p->r->l->bconst = p->l->iconst <= p->r->l->iconst;
            return interpretCompareExpr(p->r);
        case GEQ:
            p->r->l->n = BOOLTYPE;
            p->r->l->bconst = p->l->iconst >= p->r->l->iconst;
            return interpretCompareExpr(p->r);
        case EQ:
        case NEQ:
            p->r->bconst = interpretCompareExpr(p->r);
        default:
            break;
    }
    
    //check if both node types are the same
    if(p->l->n != p->r->n){
        //check if neither of them are idents
        if(p->l->t.GetTokenType() != IDENT && p->r->t.GetTokenType() != IDENT){
            interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR type mismatch in " + p->t.GetLexeme());
        }
    }

    CompareType ct = isErr;

    switch(p->t.GetTokenType()){
        case EQ:
            if(p->l->n == INTTYPE){
                return p->l->iconst == p->r->iconst;
            }else if(p->l->n == STRTYPE){
                return p->l->sconst == p->r->sconst;
            }else{
                //if neither are idents, they are just regular booleans
                if(p->l->t.GetTokenType() != IDENT && p->r->t.GetTokenType() != IDENT ){
                    return p->l->bconst == p->r->bconst;
                }
                ct = isEQ; 
                break;
            }
        case NEQ:
            if(p->l->n == INTTYPE){
                return p->l->iconst != p->r->iconst;
            }else if(p->l->n == STRTYPE){
                return p->l->sconst != p->r->sconst;
            }else{
                if(p->l->t.GetTokenType() != IDENT && p->r->t.GetTokenType() != IDENT ){
                    return p->l->bconst != p->r->bconst;
                }
                ct = isNEQ; 
                break;
            }
        case LEQ:
            if(p->l->n == INTTYPE){
                return p->l->iconst <= p->r->iconst;
            }else if(p->l->n == STRTYPE){
                return p->l->sconst <= p->r->sconst;
            }else{
                ct = isLEQ; 
                break;
            }
        case GEQ:
            if(p->l->n == INTTYPE){
                return p->l->iconst >= p->r->iconst;
            }else if(p->l->n == STRTYPE){
                return p->l->sconst >= p->r->sconst;
            }else{
                ct = isGEQ;
                break;
            }
        case LT:
            if(p->l->n == INTTYPE){
                return p->l->iconst < p->r->iconst;
            }else if(p->l->n == STRTYPE){
                return p->l->sconst < p->r->sconst;
            }else{
                ct = isLT;
                break;
            }
        case GT:
            if(p->l->n == INTTYPE){
                return p->l->iconst > p->r->iconst;
            }else if(p->l->n == STRTYPE){
                return p->l->sconst > p->r->sconst;
            }else{
                ct = isGT;
                break;
            }
        default:
            break;
    }

    //If we are here then at least one of them is an IDENT
    return compareIdents(p, ct);
}

bool compareIdents(ParseTree *p, CompareType ct){
    if(p->l->t.GetTokenType() == IDENT && p->r->t.GetTokenType() == IDENT){
        //both idents exist 
        if(hasSymbol(p->l->t.GetLexeme()) && hasSymbol(p->r->t.GetLexeme())){
            //TODO: Check if values match
        }else{
            //TODO: Error handling 
        }
    }else if(p->l->t.GetTokenType() == IDENT && p->r->t.GetTokenType() != IDENT){
        //left child is an ident
        if(hasSymbol(p->l->t.GetLexeme())){
            //check if value type of symbol matches the type of token
            if(getValue(p->l->t.GetLexeme())->vt == isInt && p->r->n == INTTYPE){
                //for comparing INTS
                switch(ct){
                    case isEQ:
                        return getValue(p->l->t.GetLexeme())->ival == p->r->iconst;
                    case isNEQ:
                        return getValue(p->l->t.GetLexeme())->ival != p->r->iconst;
                    case isGT:
                        return getValue(p->l->t.GetLexeme())->ival > p->r->iconst;
                    case isGEQ:
                        return getValue(p->l->t.GetLexeme())->ival >= p->r->iconst;
                    case isLT:
                        return getValue(p->l->t.GetLexeme())->ival < p->r->iconst;
                    case isLEQ:
                        return getValue(p->l->t.GetLexeme())->ival <= p->r->iconst;
                    case isErr:
                        break;
                }
            }else if(getValue(p->l->t.GetLexeme())->vt == isStr && p->r->n == STRTYPE){
                //for comparing STRINGS 
                switch(ct){
                    case isEQ:
                        return getValue(p->l->t.GetLexeme())->ival == p->r->iconst;
                    case isNEQ:
                        return getValue(p->l->t.GetLexeme())->ival != p->r->iconst;
                    case isGT:
                        return getValue(p->l->t.GetLexeme())->ival > p->r->iconst;
                    case isGEQ:
                        return getValue(p->l->t.GetLexeme())->ival >= p->r->iconst;
                    case isLT:
                        return getValue(p->l->t.GetLexeme())->ival < p->r->iconst;
                    case isLEQ:
                        return getValue(p->l->t.GetLexeme())->ival <= p->r->iconst;
                    case isErr:
                        break;
                }
            }else if(getValue(p->l->t.GetLexeme())->vt == isBool && p->r->n == BOOLTYPE){
                //for comparing BOOLEANS  
                switch(ct){
                    case isEQ:
                        return getValue(p->l->t.GetLexeme())->ival == p->r->iconst;
                    case isNEQ:
                        return getValue(p->l->t.GetLexeme())->ival != p->r->iconst;
                    default:
                        break; 
                }
            }else{
                //TODO: Error handling 
            }
        }else{
            //the symbols dont exist in symtable
            //TODO: Implement checking
        }    
    }else if(p->l->t.GetTokenType() != IDENT && p->r->t.GetTokenType() == IDENT){
        //right child is an ident
        //TODO: Error handling 
    }

    return false; 
}

void printOperation(ParseTree *p){
    //First check if both operands are ints or strings

    //TODO: restructure this such that ther is only ONE check of if the left child
    //is an ident, and nested checks for the type of right child
    //such as INTTYPE, ERRTYPE, or BOOLTYPE
    if(p->l->n == INTTYPE && p->r->n == INTTYPE){
        //for more than one operation, we interpret each subtree where there is more than one operation
        cout << interpretInts(p) << endl;
    }else if(p->l->n == STRTYPE && p->r->n == STRTYPE){
        cout << interpretStrings(p) << endl;
    }else if((p->l->n == STRTYPE && p->r->n == INTTYPE) || (p->r->n == STRTYPE && p->l->n == INTTYPE)){
        //one is a string and the other is an int
        cout << interpretStrings(p) << endl;
    }else if(p->l->t.GetTokenType() == IDENT && p->r->t.GetTokenType() == IDENT){
        //both are idents
        cout << interpretInts(getValue(p->l->t.GetLexeme()), getValue(p->r->t.GetLexeme()), p) << endl;
    }else if(p->l->t.GetTokenType() == IDENT && p->r->t.GetTokenType() != IDENT){
        //left child is an ident, but right isnt
        cout << interpretInts(getValue(p->l->t.GetLexeme()), p) << endl;
    }else if(p->l->t.GetTokenType() != IDENT && p->r->t.GetTokenType() == IDENT){
        //right child is an ident, but left isnt
        cout << interpretInts(getValue(p->r->t.GetLexeme()), p) << endl;
    }else if(p->l->n == INTTYPE && p->r->n == ERRTYPE){
        //left child is an int but the right is not (likely a negative int)
        cout << interpretInts(p) << endl;
    }else if(p->l->n == ERRTYPE && p->r->n == INTTYPE){
        //right child is an int but the left is not (likely a negative int)
        cout << interpretInts(p) << endl;
    }else if(p->l->n == INTTYPE && p->r->n == BOOLTYPE){
        //check the case where we need to apply unary minus
        if(interpretUnaryMinus(p)){
            cout << "True" << endl;
        }else{
            cout << "False" << endl;
        }
    }else if(p->l->n == STRTYPE && p->r->n == ERRTYPE){
        if(p->l->t.GetTokenType() == IDENT){
            if(hasSymbol(p->l->t.GetLexeme())){
               cout << interpretStrings(getValue(p->l->t.GetLexeme()), p->r);
            }else{
                //TODO:Error handling 
            }
        }
    }else{
        //TODO: Error handling
    }
}

bool interpretUnaryMinus(ParseTree *p){
    //we know p->l is -1, so we need to check p->r for a valid bool expression
    if(p->r->n == BOOLTYPE){
        if(p->r->t.GetTokenType() == IDENT){
            //TODO: Implement checks for idents
        
        }else{
            //its a boolean 
            return !p->r->bconst;
        }
        
    }else{
        //TODO: Error handling 
    }
    return false;
}

void interpretOperation(ParseTree *p, Value *v){
    //First check if both operands are just ints or strings

    if(p->l->n == INTTYPE && p->r->n == INTTYPE){
        v->ival = interpretInts(p);
        v->vt = isInt;
    }else if(p->l->n == STRTYPE && p->r->n == STRTYPE){
        v->sval = interpretStrings(p);
        v->vt = isStr;
    }else if((p->l->n == STRTYPE && p->r->n == INTTYPE) || (p->l->n == INTTYPE && p->r->n == STRTYPE)){
        //Only one of them is a string
        v->sval = interpretStrings(p);
        v->vt = isStr;
    }
    
    //Check first if both are IDENTS
    if(p->l->t.GetTokenType() == IDENT && p->r->t.GetTokenType() == IDENT){
        //check if both symbols exist in symbol table
        if(hasSymbol(p->l->t.GetLexeme()) && hasSymbol(p->r->t.GetLexeme())){
           switch(getValue(p->l->t.GetLexeme())->vt) {
                case isInt:
                    if(getValue(p->r->t.GetLexeme())->vt == isInt){
                        v->ival = interpretInts(getValue(p->l->t.GetLexeme()), getValue(p->r->t.GetLexeme()), p);
                        v->vt = isInt;
                    }else{
                        interpretError(p->r->t.GetLinenum(), "RUNTIME ERROR type mismatch in " + p->t.GetLexeme());
                    }
                    break;
                case isStr:
                    if(getValue(p->r->t.GetLexeme())->vt == isStr){
                        v->sval = interpretStrings(getValue(p->l->t.GetLexeme()), getValue(p->r->t.GetLexeme()),p);
                        v->vt = isStr;
                    }else{
                        //TODO: Error handling 
                    }
                    break;
                default:
                    //TODO: Error handling 
                    break;
           }
        }else{
            //TODO: Error handling 
        }
    }else if(p->l->t.GetTokenType() == IDENT && p->r->t.GetTokenType() != IDENT){
        //The left child is an ident but the right isnt
        //check if left child exists in symbol table
        if(hasSymbol(p->l->t.GetLexeme())){
           //check if it's either an int or a string  
           switch(getValue(p->l->t.GetLexeme())->vt) {
                case isInt:
                    v->ival = interpretInts(getValue(p->l->t.GetLexeme()), p);
                    v->vt = isInt;
                    break;
                case isStr:
                    //have interpretStrings deal with whether the right child is an int or a string
                    v->sval = interpretStrings(getValue(p->l->t.GetLexeme()), p);
                    v->vt = isStr;
                    break;
                default:
                    //TODO: Error handling 
                    break;
           }
        }else{
            //TODO: Error handling 
        }
    }else if(p->l->t.GetTokenType() != IDENT && p->r->t.GetTokenType() == IDENT){
        //The right child is an ident but the left isnt
        if(hasSymbol(p->r->t.GetLexeme())){
           //check if it's either an int or a string  
           switch(getValue(p->r->t.GetLexeme())->vt) {
                case isInt:
                    v->ival =interpretInts(getValue(p->r->t.GetLexeme()), p);
                    v->vt = isInt;
                    break;
                case isStr:
                    v->sval = interpretStrings(getValue(p->r->t.GetLexeme()), p);
                    v->vt = isStr;
                    break;
                default:
                    //TODO: Error handling 
                    break;
           }
        }else{
            //TODO: Error handling 
        }
    }else{
        //TODO: Error handling 
    }
}

int interpretInts(ParseTree *p){
    //take into account in each case if either the left or right child is a negative number 
    //by checking if its token type is a star

    //also check if the right child contains an additional operation
    if(p->r->n != INTTYPE){
        switch(p->r->t.GetTokenType()){
            case PLUS:
                return p->l->iconst + interpretInts(p->r);
            case MINUS:
                p->r->l->iconst = p->l->iconst - p->r->l->iconst;
                return interpretInts(p->r);
            case STAR:
                //if we're dealing with a negative number instead of standard multiplication
                if(p->r->l->iconst == -1)
                    break;
                return p->l->iconst * interpretInts(p->r);
            case SLASH:
                p->r->l->iconst = p->l->iconst / p->r->l->iconst;
                return interpretInts(p->r);
            default:
                break;
        }
    }

    if(p->t.GetTokenType() == PLUS){
        if(p->l->t.GetTokenType() == STAR){
            return interpretInts(p->l) + p->r->iconst;
        }else if(p->r->t.GetTokenType() == STAR){
            return interpretInts(p->r) + p->l->iconst;
        }else{
            return p->l->iconst + p->r->iconst; 
        }
    }else if(p->t.GetTokenType() == MINUS){
        if(p->l->t.GetTokenType() == STAR){
            return interpretInts(p->l) - p->r->iconst;
        }else if(p->r->t.GetTokenType() == STAR){
            return p->l->iconst - interpretInts(p->r);
        }else{
            return p->l->iconst - p->r->iconst; 
        }
    }else if(p->t.GetTokenType() == STAR){
        if(p->l->t.GetTokenType() == STAR){
            return interpretInts(p->l) * p->r->iconst;
        }else if(p->r->t.GetTokenType() == STAR){
            return interpretInts(p->r) * p->l->iconst;
        }else{
            return p->l->iconst * p->r->iconst; 
        }
    }else{
        if(p->l->t.GetTokenType() == STAR){
            return interpretInts(p->l) / p->r->iconst;
        }else if(p->r->t.GetTokenType() == STAR){
            return interpretInts(p->r) / p->l->iconst;
        }else{
            return p->l->iconst / p->r->iconst; 
        }
    }
}

int interpretInts(Value *v, ParseTree *p){
    //consider whether the right child or left child of p is an INTTYPE
    if(p->r->n == INTTYPE){
        //right child is the int
        if(p->t.GetTokenType() == PLUS){
            return v->ival + p->r->iconst; 
        }else if(p->t.GetTokenType() == MINUS){
            return v->ival - p->r->iconst; 
        }else if(p->t.GetTokenType() == STAR){
            return v->ival * p->r->iconst; 
        }else{
            return v->ival / p->r->iconst; 
        }
    }else{
        //left child is the int
        if(p->t.GetTokenType() == PLUS){
            return p->l->iconst + v->ival;
        }else if(p->t.GetTokenType() == MINUS){
            return p->l->iconst - v->ival; 
        }else if(p->t.GetTokenType() == STAR){
            return p->l->iconst * v->ival; 
        }else{
            return p->l->iconst / v->ival; 
        }
    }
    
    return 0;
}

int interpretInts(Value *v1, Value *v2, ParseTree *p){
    if(p->t.GetTokenType() == PLUS){
        return v1->ival + v2->ival; 
    }else if(p->t.GetTokenType() == MINUS){
        return v1->ival - v2->ival;
    }else if(p->t.GetTokenType() == STAR){
        return v1->ival * v2->ival;
    }else{
        return v1->ival / v2->ival;
    }
}


string interpretStrings(ParseTree *p){
    if(p->t.GetTokenType() != PLUS){
        //Check if it's not multiplication
        if(p->t.GetTokenType() != STAR){
            interpretError(p->t.GetLinenum(), "RUNTIME ERROR invalid operation involving strings");
        }else{
            //check if both are strings
            if(p->l->t.GetTokenType() == SCONST && p->r->t.GetTokenType() == SCONST ){
                interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR undefined operation between strings");
            }
            
            //one of them is a string and the other an int 
            string orig;
            if(p->l->n == STRTYPE){
                //left child is the string, right is the int 
                orig = p->l->sconst;
                for(int i = 0; i < p->r->iconst; i++){
                    if(i == p->r->iconst - 1){
                        return p->l->sconst;
                    }
                    p->l->sconst += orig; 
                    
                }
                return "";
            }else{
                //right child is the string, left is the int 
                orig = p->r->sconst;
                for(int i = 0; i < p->l->iconst; i++){
                    if(i == p->l->iconst - 1){
                        return p->r->sconst;
                    }
                    p->r->sconst += orig; 
                }
                return "";
            }
        }
    }

    //check if either are not strings
    if(p->l->t.GetTokenType() != SCONST || p->r->t.GetTokenType() != SCONST ){
        interpretError(p->l->t.GetLinenum(), "RUNTIME ERROR invalid operand in string addition");
    }

    return p->l->sconst + p->r->sconst; 
}

string interpretStrings(Value *v, ParseTree *p){
    //check if p->t is multiplication
    if(p->t.GetTokenType() == STAR){
     
    }else{
        //TODO: Error handling 
    }
    return "";
}

string interpretStrings(Value *v1, Value* v2, ParseTree *p){
    //STUB
    return "";
}

