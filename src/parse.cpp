#include "../inc/parse.h"

namespace Parser{
    bool pushed_back = false;
    Token pushed_token;

    static Token GetNextToken(istream *in, int *line){
        if(pushed_back){
            pushed_back = false;
            return pushed_token;
        }
        return getNextToken(in, line);
    }

    static void PushBackToken(Token& t){
        if(pushed_back){
            abort();
        }
        pushed_back = true;
        pushed_token = t;
    }
}

static int error_count = 0;

void ParseError(int *line, string msg){
    ++error_count;
    cout << *line << ": " << msg << endl;
}



ParseTree *Prog(istream *in, int *line){
    ParseTree *p = Slist(in, line);


    if(!p){
        ParseError(line, "No statements in program");
    }

    if(error_count){
        return 0;
    }

    return p;
}

ParseTree *Slist(istream *in, int *line){
    Token t;
    ParseTree *p = new ParseTree;
    p->l = Stmt(in, line);

    if(!p->l){
        delete(p);
        return 0;
    }

    //check if next token is a semicolon
    t = Parser::GetNextToken(in, line);

    if(t.GetTokenType() != SC){
        ParseError(line, "Missing semicolon after statement");
        return 0;
    }

    //call statementlist recursively until we no longer have statements
    p->r = Slist(in, line);

    return p;
}

ParseTree *Stmt(istream *in, int *line){
    ParseTree *p = 0;
    //check for whether the statement is an if, print, or normal expr
    Token t = Parser::GetNextToken(in, line);

    switch(t.GetTokenType()){
        case IF: 
            //Put back the token so that we can properly store it as the parent of this subtree
            Parser::PushBackToken(t);
            p = IfStmt(in, line);
            break;

        case PRINT:
            //Same as IF
            Parser::PushBackToken(t);
            p = PrintStmt(in, line);
            break;

        case DONE: 
            break;

        case ERR: 
            ParseError(line, "Invalid token");
            break;

        default:
            //put back the token and see if it's an expr
            Parser::PushBackToken(t);
            p =  Expr(in, line);
            if(!p){
                return 0;
            }
            break;
    }

    //Store the checked token

    return p;
}

ParseTree *IfStmt(istream *in, int *line){
    //We know the IF token exists
    //Get the IF token back again and store it inside of this node
    ParseTree *p = new ParseTree;
    p->t = Parser::GetNextToken(in, line);

    //Check if we have a valid expr
    Token t;
    p->l = Expr(in, line);


    if(!p){
        ParseError(line, "Invalid IF statement. No expression following keyword");
        return 0;
    }else if(HasErrType(p)){
        ParseError(line, "Invalid expression for IF statement. It must evaluate to a boolean");
        return 0;
    }

    //if we have a valid expression after IF keyword
    t = Parser::GetNextToken(in, line);

    if(t != THEN){
        ParseError(line, "Invalid IF statement. Missing THEN keyword");
        delete(p);
        return 0;
    }

    p->r = Stmt(in, line);

    if(!p->l){
        ParseError(line, "Invalid IF statement. Missing clause");
        delete(p);
        return 0;
    }

    return p;
}

ParseTree *PrintStmt(istream *in, int *line){
    //We know there is a PRINT token as we just put it back
    //Create a new tree which holds the PRINT token
    ParseTree *p = new ParseTree;
    p->t = Parser::GetNextToken(in, line);

    p->r = Expr(in, line);

    if(!p->r){
        ParseError(line, "Missing or invalid statement after PRINT");
    }

    return p;
}

ParseTree *Expr(istream *in, int *line){
    Token t;
    ParseTree *p = new ParseTree;
    p->l = LogicExpr(in, line);

    if(!p->l){
        delete(p);
        return 0;
    }

    //check if next token is a valid assignment operator
    t = Parser::GetNextToken(in, line);

    if(t != ASSIGN){
        Parser::PushBackToken(t);
        return p->l;
    }else{
        p->t = t;
    }

    //we have a valid assignment operator. check if we have a valid expression following it

    p->r = Expr(in, line);

    if(!p->r){
        ParseError(line, "Invalid expression after assignment");
        delete(p);
        delete(p->l);
        return 0;
    }

    return p;
}

ParseTree *LogicExpr(istream *in, int *line){
    ParseTree *p = new ParseTree;
    Token t;
    p->l = CompareExpr(in, line);

    if(!p->l){
        delete(p);
        return 0;
    }
    //check if the next token is either AND or OR
    t = Parser::GetNextToken(in, line);

    if(t != LOGICAND && t != LOGICOR){
        Parser::PushBackToken(t);
        return p->l;
    }else{
        //store the logic token inside parent
        p->t = t;
    }


    //t-l is valid,check for a second expr
    p->r = LogicExpr(in, line);

    if(!p->r){
        ParseError(line, "Invalid statement. Missing expression after operator");
        delete(p->l);
        delete(p);
        return 0;
    }

    return p;
}

ParseTree *CompareExpr(istream *in, int *line){
    ParseTree *p = new ParseTree;
    Token t;

    p->l = AddExpr(in, line);

    //check if addexpr is valid
    if(!p->l){
        delete(p);
        return 0;
    }

    //check for valid token
    t = Parser::GetNextToken(in, line);

    switch(t.GetTokenType()){
        case EQ: 
        case NEQ: 
        case LT: 
        case LEQ: 
        case GT:
        case GEQ:
            //store the comparison token in parent
            p->t = t;
            break;
        default:
            Parser::PushBackToken(t);
            return p->l;
    }

    //check for valid addexpr after operation
    p->r = CompareExpr(in, line);

    if(!p->r){
        ParseError(line, "Invalid comparison. Missing or invalid expression after operator");
        delete(p);
        delete(p->l);
        return 0;
    }

    return p;
}

ParseTree *AddExpr(istream *in, int *line){
    ParseTree *p = new ParseTree;
    Token t;

    p->l = MulExpr(in, line);

    if(!p->l){
        delete(p);
        return 0;
    }


    //Mulexpr is valid check if next token is either a PLUS or MINUS
    //OPTIONALLY followed by a PLUS or a MINUS
    t = Parser::GetNextToken(in, line);

    if(t != PLUS && t != MINUS){
        Parser::PushBackToken(t);
        return p->l;
    }else{
        //store the PLUS or MINUS token in parent
        p->t = t;
    }

    p->r = AddExpr(in, line);

    if(!p->r){
        ParseError(line, "Missing or invalid expression after addition/subtraction");
        delete(p);
        return 0;
    }

    return p;
}

ParseTree *MulExpr(istream *in, int *line){
    ParseTree *p = new ParseTree;
    Token t;
    p->l = Factor(in, line);

    if(!p->l){
        delete(p);
        return 0;
    }

    //Optionally followed by a STAR or SLASH
    t = Parser::GetNextToken(in, line);

    if(t != STAR && t != SLASH){
        Parser::PushBackToken(t);
        return p->l;
    }else{
        //store the STAR or SLASH token in parent
        p->t = t;
    }

    //Next expression must be valid
    p->r = MulExpr(in, line);

    if(!p->r){
        ParseError(line, "Invalid or missing Factor");
        delete(p);
        return 0;
    }

    return p;
}

ParseTree *Factor(istream *in, int *line){
    //check for negative sign before checking for primary
    ParseTree *p = new ParseTree;
    Token t = Parser::GetNextToken(in, line);

    if(t == MINUS){
        //because we handle a negative number as -1 * the_number
        //make the parent's token STAR
        p->t = *new Token(STAR, "*", *line);

        //we store the original, non-negative number in the right child
        p->r = Primary(in, line);

        //check if p->r is valid first then if it's either a boolean or integer
        if(p->r && (p->r->n == INTTYPE || p->r->n == BOOLTYPE)){
            p->l = new ParseTree;
            p->l->t = *new Token(ICONST, "-1", *line);
            p->l->n = INTTYPE;
            p->l->iconst = -1;
        }else{
            ParseError(line, "Invalid use of minus sign");
            return 0;
        }
    }else{
        Parser::PushBackToken(t);
        p = Primary(in, line);
    }


    if(!p){
        ParseError(line, "Missing Primary token for statement");
        return 0;
    }

    return p;
}

ParseTree *Primary(istream *in, int *line){
    ParseTree *p = new ParseTree;
    Token t = Parser::GetNextToken(in, line);
    Token t2;

    switch(t.GetTokenType()){
        case TRUE: 
            p->bconst = true;
            p->n = BOOLTYPE;
            break;
        case FALSE:
            p->bconst = false;
            p->n = BOOLTYPE;
            break;
        case IDENT:
            p->n = BOOLTYPE;
            break;
        case ICONST:
            p->iconst = stoi(t.GetLexeme());
            p->n = INTTYPE;
            break;
        case SCONST:
            p->sconst = t.GetLexeme();
            p->n = STRTYPE;
            break;
        case LPAREN:
            //Handle if there is a valid expression within the parenthesis
            p->r = Expr(in, line);
            //check if there is a matching right parent
        case RPAREN:{
            t2 = Parser::GetNextToken(in, line);
            if(t2 != RPAREN){
                ParseError(line, "Invalid Expression. Must have matching parenthesis");
                return 0;
            }
            //while there is a valid right child, keep going until we cant
            ParseTree *tmp = p->r;
            while(tmp->r){
                tmp = tmp->r;
            }
            tmp->r = new ParseTree; 
            tmp->r->t = t2;
            break;
        }
        default:
            delete(p);
            return 0;
    }

    p->t = t;
    return p;
}

