#include <istream>
#include <stdio.h>
#include <algorithm>
#include "../inc/tokens.h"

using namespace std;

Token getNextToken(istream *in, int *linenum){
  enum LexState {
		 BEGIN,
		 INIDENT,
		 ININTEGER,
		 INSTRING,
		 INCOMMENT,
		 INSPECIAL
  };

  LexState lexstate = BEGIN;
  string lexeme = "";
  char ch;
  TokenType tt;
  
  while(in->get(ch)){

    if(ch == '\n'){
      (*linenum)++;
    }

    switch(lexstate){

    case BEGIN:
      
      if(isspace(ch)){
	continue;
      }
      
      lexeme += ch;

      if(isalpha(ch)){
	lexstate = INIDENT;
	tt = IDENT;
	goto INIDENT;
      }else if (isdigit(ch)){
	lexstate = ININTEGER;
	tt = ICONST;
	goto ININTEGER;
      }else if(ch == '"'){
	lexstate = INSTRING;
	tt = SCONST;
      }else if(ch == '#'){
	lexstate = INCOMMENT;
      }else{
	lexstate = BEGIN;
	goto INSPECIAL;
      }
      continue;

    case INIDENT:
      lexeme += ch;
    INIDENT:
      if(isspace(ch)){
	goto CHECK_KEYWORD;
      }else if(!isalpha(in->peek()) && !isdigit(in->peek())){
	goto CHECK_KEYWORD;
      }
      continue;

    case ININTEGER:
      lexeme += ch;
    ININTEGER:
      if(isspace(in->peek())){
	goto DONE;
      }else if(isalpha(in->peek())){
	in->get(ch);
	lexeme += ch;
	goto ERROR;
      }else if(!isdigit(in->peek()) && !isalpha(in->peek())){
	goto DONE;
      }
      continue;

    case INSTRING:
      lexeme += ch;
      if(ch == '"'){
	lexeme.erase(std::remove(lexeme.begin(), lexeme.end(), '"'), lexeme.end());
	goto DONE;
      }else if(ch == '\n'){
	goto ERROR;
      }
      continue;

    case INCOMMENT:
      if(ch == '\n'){
	lexstate = BEGIN;
	lexeme = "";
      }
      continue;
      
    case INSPECIAL:
      switch(ch){
      case '=':
	tt = NEQ;
	goto DONE;
      case '&':
	tt = LOGICAND;
	goto DONE;
      case '|':
	tt = LOGICOR;
	goto DONE;
      default:
	goto ERROR;
      }

    INSPECIAL:
      //for single character operators and tokens
      switch(ch){
      case '+':
	tt = PLUS;
	goto DONE;
      case '-':
	tt = MINUS;
	goto DONE;
      case '*':
	tt = STAR;
	goto DONE;
      case '/':
	tt = SLASH;
	goto DONE;
      case '=':
	if(in->peek() == '='){
	  tt = EQ;
	  in->get(ch);
	  lexeme += ch;
	}else{
	  tt = ASSIGN;
	}
	goto DONE;
      case '<':
	if(in->peek() == '='){
	  tt = LEQ;
	  in->get(ch);
	  lexeme += ch;
	  goto DONE;
	}else{
	  tt = LT;
	  goto DONE;
	}
      case '>':
	if(in->peek() == '='){
	  tt = GEQ;
	  in->get(ch);
	  lexeme += ch;
	  goto DONE;
	}else{
	  tt = GT;
	  goto DONE;
	}
      case '(':
	tt = LPAREN;
	goto DONE;
      case ')':
	tt = RPAREN;
	goto DONE;
      case ';':
	tt = SC;
	goto DONE;
	//for double character operators
      case '!':
      case '&':
      case '|':
	lexstate = INSPECIAL;
	continue;
      }

    ERROR:
      return *new Token(ERR, lexeme, *linenum);

      //check if lexeme is a keyword
    CHECK_KEYWORD:
      if(lexeme == "print"){
	tt = PRINT;
      }else if(lexeme == "if"){
	tt = IF;
      }else if(lexeme == "then"){
	tt = THEN;
      }else if(lexeme == "true"){
	tt = TRUE;
      }else if(lexeme == "false"){
	tt = FALSE;
      }
	 
    DONE:
      lexstate = BEGIN;
      return *new Token(tt, lexeme, *linenum);
    }
  }

  return *new Token(DONE, lexeme, *linenum);
}

