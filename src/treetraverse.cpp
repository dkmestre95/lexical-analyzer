#include "../inc/parse.h"

int LeafCount(ParseTree *p){
  int leaf_count = 0;

  if(p->l){
    leaf_count += LeafCount(p->l);
  }

  if(p->r){
    leaf_count += LeafCount(p->r);
  }

  if(!p->l && !p->r){
    leaf_count++;
  }

  return leaf_count;
}

int StringCount(ParseTree *p){
  int str_count = 0;

  if(p->l){
    str_count += StringCount(p->l);
  }
  if(p->r){
    str_count += StringCount(p->r);
  }

  if(p->n == STRTYPE){
    str_count++;
  }
  
  return str_count;
}

int IdentCount(ParseTree *p, map<string, int>& m){
  int ident_count = 0;

  if(p->l){
    ident_count += IdentCount(p->l, m);
  }
  if(p->r){
    ident_count += IdentCount(p->r, m);
  }

  if(p->t == IDENT){
    ident_count++;
    //add ident to list
    m[p->t.GetLexeme()]++;
  }
  
  return ident_count;
}

//check recursively for a boolean expression

bool HasErrType(ParseTree *p){
  bool hasErrType;

  if(p->l){
    hasErrType = HasErrType(p->l);
    if(!hasErrType){
      return false;
    }
  }

  if(p->r){
    hasErrType = HasErrType(p->r);
    if(!hasErrType){
      return false;
    }
  }

  return (p->n == ERRTYPE);
}
