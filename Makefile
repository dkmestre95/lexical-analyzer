# -*-MakeFile-*-
#Variables
CPP := $(shell ls src/*.cpp)
TMP := $(subst src/,obj/,$(CPP))
OBJ := $(subst .cpp,.o,$(TMP))
INC := $(shell ls inc/*.h*)
TIN := $(shell ls test/in/*)
TOUT := $(subst in/*,out/*.correct,$(TIN))
CXX := clang++
CXXFLAGS := -Wall -g -std=c++11 -I inc/

OUT := bin/prog4

all: $(OUT)

$(OUT): $(OBJ) $(INC)
	$(CXX) $(CXXFLAGS) $(OBJ) -o $(OUT)

#Patterns
obj/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@
#Cleaning
clean:
	rm -f obj/* $(OUT)

re: clean all
#Testing

test: all $(TIN)

test/in/%: test/out/%.correct
	$(OUT) $@ > out
	diff $< out
#Phony Targers
.PHONY: all 
